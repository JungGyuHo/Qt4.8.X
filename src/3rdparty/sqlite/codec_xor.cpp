#if !defined(SQLITE_CODEC_BLOWFISH) && !defined(SQLITE_CODEC_AES256)

#include <memory.h>

#include "codec.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tagST_CODEC_HANDLE
{
	int dummy;
} ST_CODEC_HANDLE, *LPST_CODEC_HANDLE;

HANDLE_CODEC SQLiteCodecInit(IN_PARAM const void* pKey, IN_PARAM int nCbKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize)
{
	LPST_CODEC_HANDLE pstCodecHandle = NULL;

	pstCodecHandle = new ST_CODEC_HANDLE;
	memset(pstCodecHandle, '\0', sizeof(ST_CODEC_HANDLE));
	pstCodecHandle->dummy = 333;

	return pstCodecHandle;
}

void SQLiteCodecDeInit(IN_PARAM HANDLE_CODEC hHandle)
{
	if (NULL != hHandle)
	{
		delete hHandle;
		hHandle = NULL;
	}
}

int SQLiteCodecEncode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	INT i = 0;

	for (i=0; i<nCbSource; i++)
	{
		((char*)pDest)[i] = ((char*)pSource)[i] ^ 255;
	}

	*pnCbDest = nCbSource;

	return 1;
}

int SQLiteCodecDecode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	INT i = 0;

	for (i=0; i<nCbSource; i++)
	{
		((char*)pDest)[i] = ((char*)pSource)[i] ^ 255;
	}

	*pnCbDest = nCbSource;

	return 1;
}

#ifdef __cplusplus
}
#endif

#endif

