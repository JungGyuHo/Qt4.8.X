#ifdef SQLITE_CODEC_BLOWFISH

#include <memory.h>

#include "codec.h"
#include "blowfish.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tagST_CODEC_HANDLE
{
	// Blowfish instance가 들어간다.
	Blowfish *pBF;
} ST_CODEC_HANDLE, *LPST_CODEC_HANDLE;

HANDLE_CODEC SQLiteCodecInit(IN_PARAM const void* pKey, IN_PARAM int nCbKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize)
{
	void*				pRtnValue		= NULL;
	LPST_CODEC_HANDLE	pstCodecHandle	= NULL;
	char*				lpszKey			= NULL;

	pstCodecHandle = new ST_CODEC_HANDLE;
	if (NULL == pstCodecHandle)
	{
		assert(FALSE);
		goto FINAL;
	}

    memset( pstCodecHandle, '\0', sizeof(ST_CODEC_HANDLE) );

	lpszKey = new char[nCbKey+1];
	if (NULL == lpszKey)
	{
		assert(FALSE);
		goto FINAL;
	}
    
    memset( lpszKey, '\0', sizeof(char)*(nCbKey+1) );
    #if defined( _MSC_VER )
    strncpy_s( lpszKey, nCbKey+1, (char*)pKey, _TRUNCATE );
    #else
    strncpy( lpszKey,  (char*)pKey , nCbKey+1 );
    #endif

	// Blowfish instance를 만들고, password 세팅한다.
	pstCodecHandle->pBF = new Blowfish;
	pstCodecHandle->pBF->Set_Passwd(lpszKey);

	// blowfish block 크기
	// 알고리즘 특징상 input size와 output size가 같다.
	// 단, size는 8의 배수배 단위여야 한다.
	// (sqlite의 page 단위가 2^n이고, 512보다 크므로, 8byte 단위임이 확실하기 때문이다.)
	*punCbBlockSize = 0;

	pRtnValue = pstCodecHandle;

FINAL:
	if (NULL != lpszKey)
	{
		delete [] lpszKey;
		lpszKey = NULL;
	}
	return pRtnValue;
}

void SQLiteCodecDeInit(IN_PARAM HANDLE_CODEC hHandle)
{
	LPST_CODEC_HANDLE pstHandle = NULL;

	pstHandle = (LPST_CODEC_HANDLE)hHandle;
	if (NULL == pstHandle)
	{
		goto FINAL;
	}

	if (NULL != pstHandle->pBF)
	{
		delete pstHandle->pBF;
		pstHandle->pBF = NULL;
	}

	delete hHandle;
	hHandle = NULL;

FINAL:
	return;
}

int SQLiteCodecEncode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	int					nRtnValue	= 1;
	LPST_CODEC_HANDLE	pstHandle	= NULL;
	unsigned int		dwCbLength	= 0;

	pstHandle = (LPST_CODEC_HANDLE)hHandle;
	if (NULL == pstHandle)
	{
		nRtnValue = 0;
		assert(FALSE);
		goto FINAL;
	}

	if (0 == nCbSource % 8)
	{
		// good
	}
	else
	{
		// bad
		// blowfish는 8byte 단위가 들어와야 한다.
		assert(FALSE);
		goto FINAL;
	}

	// 암호화!
	dwCbLength = nCbSource;
	memcpy(pDest, pSource, nCbDestBufSize);
	pstHandle->pBF->Encrypt(pDest, dwCbLength);
	*pnCbDest = dwCbLength;

FINAL:
	return nRtnValue;
}

int SQLiteCodecDecode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	int					nRtnValue	= 1;
	LPST_CODEC_HANDLE	pstHandle	= NULL;
	unsigned int     	dwCbLength	= 0;

	pstHandle = (LPST_CODEC_HANDLE)hHandle;
	if (NULL == pstHandle)
	{
		nRtnValue = 0;
		assert(FALSE);
		goto FINAL;
	}

	if (0 == nCbSource % 8)
	{
		// good
	}
	else
	{
		// bad
		// blowfish는 8byte 단위가 들어와야 한다.
		assert(FALSE);
		goto FINAL;
	}

	// 복호화!
	dwCbLength = nCbSource;
    memcpy( pDest, pSource, nCbDestBufSize );
	pstHandle->pBF->Decrypt(pDest, dwCbLength);
	*pnCbDest = dwCbLength;

FINAL:
	return nRtnValue;
}

#ifdef __cplusplus
}
#endif

#endif

