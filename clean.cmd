pushd %~dp0

nmake distclean
cd bin
del *.exe
del *.manifest
cd ..
cd qmake
nmake distclean
del makefile
del *.exe
cd ..

cd src\corelib\global
del qconfig.h
del qconfig.cpp
cd ..\..\..


del configure.cache
del .qmake.cache

cd lib
del /s /f *.prl
del /s /f *.lib
cd ..

del /s /f *.pch
del /s /f *.obj
del /s /f *.pdb
del /s /f *.tmp 
del /s /f makefile.debug
del /s /f makefile.release
del /s /f makefile
del /s /f *.sln
del /s /f *.vcxproj
del /s /f *.vcxproj.filters

rd /s /q tmp
rd /s /q plugins
rd /s /q imports

cd mkspecs
rd /s /q default
del qconfig.pri
cd ..

OriginalSlns.exe -y -gm2 -silent

call deldirs tmp

popd
